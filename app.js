var express = require('express'), fs = require('fs'), http = require('http');
var app = express();
app.use(express.static('client'));
app.get('/', function(req, res) {
	res.sendfile('./templates/index.html');
});

app.get('/pages', function(req, res) {
	res.send(pages);
});

app.get('/header',function(req, res) {
	res.send({
				id : '0',
				name : 'Chris Salt',
				address : 'Flat 3, George House, George Hill, Norwich, Norfolk, UK, NR67DE',
				email : 'chris@chris-salt.com',
				phone : '07880206963'
			});
});

app.get('/pages/:id', function(req, res) {
	fs.readFile(findPage(req.params.id), 'utf8', function(err, data) {
		if (err)
			throw err;
		res.send({
			html : data
		});
	});
});

pages = [ {
	id : '0',
	file : './pages/personal_statement.html',
	name : 'Personal Statement'
}, {
	id : '1',
	file : './pages/development_skills.html',
	name : 'Development Skills'
}, {
	id : '2',
	file : './pages/employment_history.html',
	name : 'Employment History'
}, {
	id : '3',
	file : './pages/education.html',
	name : 'Education'
}, {
	id : '4',
	file : './pages/hobbies.html',
	name : 'Hobbies and Interests'
} ];

function findPage(id) {
	for (var i = 0; i < pages.length; i++) {
		if (pages[i].id === id) {
			return pages[i].file;
		}
	}
	throw "Couldn't find object with id: " + id;
}

app.listen(3000);
console.log('Listening on port 3000...');
