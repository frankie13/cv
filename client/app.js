(function() {
	var app = angular.module('cv', []);

	app.controller('PageController', function($scope, $sce) {
		$scope.$on('page-changed', function(event, args) {
			$scope.html = $sce.trustAsHtml(args.html);
		});

	});

	app.controller('MenuController', function($scope, $rootScope, $http) {
		$http.get('/pages/').success(
				function(data, status, headers, config) {
					$scope.items = data;
				});

		$scope.loadPage = function(index) {
			$http.get('/pages/' + index).success(
					function(data, status, headers, config) {
						$rootScope.$broadcast('page-changed', {
							id : index,
							html : data.html
						});
					});
		};

		$scope.loadPage(0);

	});

	app.controller('HeaderController', function($scope, $http) {
		$http.get('/header/').success(
				function(data, status, headers, config) {
					$scope.header = data;
				});

	});

})();
